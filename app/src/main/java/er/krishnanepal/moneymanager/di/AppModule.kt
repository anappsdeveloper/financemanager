package er.krishnanepal.moneymanager.di

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.google.gson.Gson
import er.krishnanepal.moneymanager.R
import er.krishnanepal.moneymanager.data.local.IncomeExpensesDao
import er.krishnanepal.moneymanager.data.local.MainDatabase
import er.krishnanepal.moneymanager.data.repo.IncomeExpensesRepo
import er.krishnanepal.moneymanager.data.repo.IncomeExpensesRepoImp
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import er.krishnanepal.moneymanager.utils.SystemPrefManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton //singleton means create instance only once
    fun provideMainDataBase(@ApplicationContext context: Context) : MainDatabase {
        return Room.databaseBuilder(
            context,
            MainDatabase::class.java,
            MainDatabase.DATABASE_NAME
        )
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @Singleton
    fun provideIncomeExpensesDao(mainDatabase: MainDatabase) : IncomeExpensesDao {
        return mainDatabase.incomeExpensesDao()
    }

    @Provides
    @Singleton
    fun provideMainRepo(incomeExpensesDao: IncomeExpensesDao): IncomeExpensesRepo {
        return IncomeExpensesRepoImp(incomeExpensesDao)
    }


    @Singleton
    @Provides
    @Named("error")
    fun  errorMessage(@ApplicationContext context: Context) = context.getString(R.string.error)

    @Singleton
    @Provides
    @Named("info")
    fun  infoMessage() = "This is info message"

    @Provides
    @Singleton
    fun provideSharedPrefs(@ApplicationContext context: Context, gson: Gson): SystemPrefManager {
        return SystemPrefManager(context, gson)
    }

  /*  @Provides
    @Singleton
    fun providesSecuredPrefManager(application: Application, gson: Gson): SecuredPrefManager {
        return SecuredPrefManager(application, gson)
    }*/

 /*   @ApplicationScope
    @Provides
    @Singleton
    fun provideApplicationScope(): CoroutineScope {
        return CoroutineScope(SupervisorJob())
    }*/

    @Singleton
    @Provides
    fun provideGson(): Gson = Gson()
}