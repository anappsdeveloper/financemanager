package er.krishnanepal.moneymanager.utils

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import java.nio.charset.StandardCharsets
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SystemPrefManager @Inject constructor(private val context: Context, private val gson: Gson){

        companion object {

            private const val ACCESS_TOKEN = "access_token"
            private const val REFRESH_TOKEN = "refresh_token"

            private const val IS_FIRST_LAUNCH = "is_first_launch"
            private const val IS_REDIRECT_TO_LOGIN = "is_redirect_to_login"
            private const val IS_INIT_BEFORE_LOGIN_FETCHED = "is_init_before_login_fetched"
            private const val IS_OTP_ENABLED = "is_otp_enabled"
            private const val IS_PASSWORD_ENABLED = "is_password_enabled"
            private const val CUSTOMER_NAME = "customer_name"
            private const val CUSTOMER_NUMBER = "customer_number"
            private const val IS_BIOMETRIC_FOR_LOGIN = "is_biometric_for_login"
            private const val IS_BIOMETRIC_FOR_TRANSACTION = "is_biometric_for_transaction"
            private const val FLIGHT_RECENT_SEARCH = "flight_recent_search"
            private const val LANGUAGE = "en"
            private const val ACCOUNT = "account"
            const val SYSTEM_THEME = "system_theme"
        }

        private var sharedPreferences: SharedPreferences =
            context.getSharedPreferences("aaaa", Context.MODE_PRIVATE)

        private val edit = sharedPreferences.edit()

        fun save(key: String, value: String?) {
            sharedPreferences.edit().putString(key, value).apply()
        }

        fun save(key: String, value: Int?) {
            if (value != null) {
                sharedPreferences.edit().putInt(key, value).apply()
            }
        }

        fun save(key: String, value: Boolean) {
            sharedPreferences.edit().putBoolean(key, value).apply()
        }

        fun save(key: String, permissionList: Set<String?>?) {
            sharedPreferences.edit().putStringSet(key, permissionList).apply()
        }

        fun save(key: String, bytes: ByteArray?) {
            sharedPreferences.edit().putString(key, String(bytes!!, StandardCharsets.ISO_8859_1))
                .apply()
        }

        fun getByte(key: String): ByteArray? {
            val str = sharedPreferences.getString(key, null)
            return str?.toByteArray(StandardCharsets.ISO_8859_1)
        }

        operator fun get(key: String?): String? {
            return sharedPreferences.getString(key, "")
        }

        fun getInt(key: String?): Int {
            return sharedPreferences.getInt(key, 0)
        }

        fun getBoolean(key: String?): Boolean {
            return sharedPreferences.getBoolean(key, false)
        }


        fun isEmpty(key: String?): Boolean {
            return sharedPreferences.getString(key, null) == null
        }

        fun clear() {
            sharedPreferences.edit().clear().apply()
        }

        fun getFirstBiometric(): Boolean {
            return sharedPreferences.getBoolean("first_biometric", true)
        }

        fun setFirstBiometric() {
            sharedPreferences.edit().putBoolean("first_biometric", false).apply()
        }

        fun getPassword(): String? {
            var password: String = sharedPreferences.getString("password", null).toString()
            var len = password.length
            len /= 2
            val b1 = StringBuilder(password.substring(0, len))
            val b2 = StringBuilder(password.substring(len))
            password = b1.reverse().toString() + b2.reverse().toString()
            return password
        }

        fun setPassword(password: String) {
            var password = password
            var len = password.length
            len /= 2
            val b1 = StringBuilder(password.substring(0, len))
            val b2 = StringBuilder(password.substring(len))
            b1.reverse()
            b2.reverse()
            password = b1.toString() + b2.toString()
            sharedPreferences.edit().putString("password", password).apply()


        }

        fun isFirstLaunch(): Boolean = sharedPreferences.getBoolean(IS_FIRST_LAUNCH, true)

        fun setIsFirstLaunch(isFirstLaunch: Boolean) {
            save(IS_FIRST_LAUNCH, isFirstLaunch)
        }

        fun isRedirectToLogin(): Boolean = getBoolean(IS_REDIRECT_TO_LOGIN)

        fun setIsRedirectToLogin(isRedirectToLogin: Boolean) {
            save(IS_REDIRECT_TO_LOGIN, isRedirectToLogin)
        }

        fun getAccessToken(): String =
            if (get(ACCESS_TOKEN).isNullOrBlank()) "" else "Bearer ${get(ACCESS_TOKEN)}"

        fun setAccessToken(accessToken: String?) {
            save(ACCESS_TOKEN, accessToken)
        }

        fun getRefreshToken(): String = get(REFRESH_TOKEN) ?: ""

        fun setRefreshToken(refreshToken: String?) {
            save(REFRESH_TOKEN, refreshToken)
        }

        fun isInitBeforeLoginFetched(): Boolean = getBoolean(IS_INIT_BEFORE_LOGIN_FETCHED)

        fun setIsInitBeforeLoginFetched(isInitBeforeLoginFetched: Boolean) {
            save(IS_INIT_BEFORE_LOGIN_FETCHED, isInitBeforeLoginFetched)
        }


        fun isOtpEnabled(): Boolean = getBoolean(IS_OTP_ENABLED)

        fun setIsOtpEnabled(isOtpEnabled: Boolean) {
            save(IS_OTP_ENABLED, isOtpEnabled)
        }

        fun isPasswordEnabled(): Boolean = getBoolean(IS_PASSWORD_ENABLED)

        fun setIsPasswordEnabled(isPasswordEnabled: Boolean) {
            save(IS_PASSWORD_ENABLED, isPasswordEnabled)
        }

        fun getCustomerName(): String? = get(CUSTOMER_NAME)

        fun setCustomerName(customerName: String?) {
            save(CUSTOMER_NAME, customerName)
        }

        fun setLanguage(language: String?) {
            save(LANGUAGE, language)
        }

        fun getLanguage(): String? = get(LANGUAGE)


        fun getCustomerNumber(): String? = get(CUSTOMER_NUMBER)

        fun setCustomerNumber(customerNumber: String?) {
            save(CUSTOMER_NUMBER, customerNumber)
        }


        fun isBiometricForLoginEnabled(): Boolean = getBoolean(IS_BIOMETRIC_FOR_LOGIN)

        fun setIsBiometricForLoginEnabled(isBiometricForLoginEnabled: Boolean) {
            save(IS_BIOMETRIC_FOR_LOGIN, isBiometricForLoginEnabled)
        }

        fun isBiometricForTransactionEnabled(): Boolean = getBoolean(IS_BIOMETRIC_FOR_TRANSACTION)

        fun setIsBiometricForTransactionEnabled(isBiometricForTransactionEnabled: Boolean) {
            save(IS_BIOMETRIC_FOR_TRANSACTION, isBiometricForTransactionEnabled)
        }

        /* fun getRecentFlightSearches(): MutableList<FlightRecentSearch>? {
             val json = sharedPreferences.getString(FLIGHT_RECENT_SEARCH, "")
             val type = object : TypeToken<MutableList<FlightRecentSearch>>() {}.type
             return gson.fromJson(json, type)
         }

         fun setRecentFlightSearches(recentSearches: MutableList<FlightRecentSearch>?) {
             recentSearches?.let {
                 val json = gson.toJson(recentSearches)
                 sharedPreferences.edit().putString(FLIGHT_RECENT_SEARCH, json).apply()
             }
         }*/

        /*fun getAccountList(): List<AccountData> {
            val json = sharedPreferences.getString(ACCOUNT, "")
            val accountResponse = Gson().fromJson(json, AccountResponse::class.java)

            return accountResponse?.data ?: mutableListOf()
        }

        fun setAccount(accountResponse: AccountResponse) {
            val json = gson.toJson(accountResponse)
            sharedPreferences.edit().putString(ACCOUNT, json).apply()
        }*/

        fun logout() {
            setAccessToken(null)
            setRefreshToken(null)

            /*      context.startActivity(Intent(context, AuthActivity::class.java).apply {
                      flags =
                          Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                  })*/
        }

}