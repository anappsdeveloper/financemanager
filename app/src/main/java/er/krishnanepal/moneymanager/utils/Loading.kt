package er.krishnanepal.moneymanager.utils

import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDialog
import er.krishnanepal.moneymanager.R

class Loading(context: Context) : AppCompatDialog(context) {

    init {
        val wlmp = window!!.attributes
        wlmp.gravity = Gravity.CENTER_HORIZONTAL or Gravity.CENTER_VERTICAL
        /*  window!!.setBackgroundDrawableResource(R.color.color_transparent)*/
        window!!.attributes = wlmp
        setTitle(null)
        setCancelable(true)
        setOnCancelListener(null)
        val params = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT
        )
        params.gravity = Gravity.CENTER_HORIZONTAL
        params.layoutDirection = View.LAYOUT_DIRECTION_LTR
        val view: View = layoutInflater.inflate(R.layout.app_loading, null)
        addContentView(view, params)
    }

}