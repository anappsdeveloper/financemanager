package er.krishnanepal.moneymanager.presentation

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import er.krishnanepal.moneymanager.ui.graph.GraphFragment

import er.krishnanepal.moneymanager.ui.home.HomeFragment
import er.krishnanepal.moneymanager.utils.enum.FragmentEnum

class ViewPagerAdapter(
    fragmentManager: FragmentManager, lifecycle: Lifecycle
) :
    FragmentStateAdapter(fragmentManager, lifecycle) {
    override fun getItemCount(): Int {
        return 2
    }

    override fun createFragment(position: Int): Fragment {
        return  when(position){
            0 -> {
                    HomeFragment()
            }else -> {
                    GraphFragment()
            }
        }
    }
}