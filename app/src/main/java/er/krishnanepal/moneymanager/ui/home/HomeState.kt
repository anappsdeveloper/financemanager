package er.krishnanepal.moneymanager.ui.home

import er.krishnanepal.moneymanager.data.local.IncomeExpensesEntity

data class HomeState(
    val isLoading: Boolean = false,
    val list:List<IncomeExpensesEntity> = emptyList(),
    val error:String = ""
)
