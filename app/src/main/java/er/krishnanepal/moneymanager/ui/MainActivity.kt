package er.krishnanepal.moneymanager.ui

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import er.krishnanepal.moneymanager.R
import er.krishnanepal.moneymanager.databinding.ActivityMainBinding

import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity(){
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        setSupportActionBar(binding.toolbar)
        val navController = findNavController(R.id.nav_host_fragment)

        val appBarConfiguration = AppBarConfiguration(
            setOf( //hide <- backarrow
                R.id.mainFragment,
                R.id.loginFragment,
                R.id.signupFragment,
                R.id.nav_home_fragment,
                R.id.nav_graph_fragment
            )
        )

        binding.toolbar.setupWithNavController(navController, appBarConfiguration)

        setupActionBarWithNavController(navController,appBarConfiguration)

        binding.bottomNavigation.setupWithNavController(navController)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.signupFragment -> hideBottomNav()
                R.id.loginFragment -> hideBottomNav()
           else -> showBottomNav()
            }
        }
    }

    fun showBottomNav(){
        binding.bottomNavigation.visibility = View.VISIBLE
    }

    fun hideBottomNav(){
        binding.bottomNavigation.visibility = View.GONE
    }

   /* override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.more_menu, menu)

    *//*    val search = menu?.findItem(R.id.search)
        val searchView = search?.actionView as? SearchView
        searchView?.isSubmitButtonEnabled = true
        searchView?.setOnQueryTextListener(this)*//*
        return true
    }*/

  /*  override fun onOptionsItemSelected(item: MenuItem): Boolean {
      *//*  val navController = findNavController(R.id.nav_host_fragment)
        return item.onNavDestinationSelected(navController) || super.onOptionsItemSelected(item)*//*

        return when (item.itemId) {
            R.id.logout -> {
                Toast.makeText(this,"Logout",Toast.LENGTH_SHORT).show()
                true
            }
            R.id.search -> {

                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }*/
}