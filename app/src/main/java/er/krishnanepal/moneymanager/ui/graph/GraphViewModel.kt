package er.krishnanepal.moneymanager.ui.graph

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import er.krishnanepal.moneymanager.base.BaseViewModel
import er.krishnanepal.moneymanager.data.local.IncomeExpensesEntity
import er.krishnanepal.moneymanager.data.model.DailyModel
import er.krishnanepal.moneymanager.domain.use_case.GetDailyIncomeExpensesUseCase
import er.krishnanepal.moneymanager.domain.use_case.GetIncomeExpensesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class GraphViewModel
@Inject constructor(private val dailyIncomeExpensesUseCase: GetDailyIncomeExpensesUseCase,
) : BaseViewModel() {

    //date wise both sum of income and expenses show
    val _dailyIncomeExpensesState = MutableStateFlow(mutableListOf<DailyModel>())
    val dailyIncomeExpensesState: StateFlow<MutableList<DailyModel>> = _dailyIncomeExpensesState

    init {
        getDailyIncomeExpenses()
    }

    fun getDailyIncomeExpenses() {
        showLoading()
        dailyIncomeExpensesUseCase().onEach {
            delay(1000)
            _dailyIncomeExpensesState.value = it as MutableList<DailyModel>
            hideLoading()
        }.launchIn(viewModelScope)
    }

}