package er.krishnanepal.moneymanager.ui

import android.app.Application
import android.content.Context
import com.facebook.stetho.BuildConfig
import com.facebook.stetho.Stetho
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BaseApplication:Application() {
    companion object {
        lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
        if(BuildConfig.DEBUG){
            //for debug
        }
        context = this
    }
}