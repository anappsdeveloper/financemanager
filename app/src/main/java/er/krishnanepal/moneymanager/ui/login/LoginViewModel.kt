package er.krishnanepal.moneymanager.ui.login

import androidx.lifecycle.ViewModel
import er.krishnanepal.moneymanager.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LoginViewModel
    @Inject constructor() : BaseViewModel() {

}
