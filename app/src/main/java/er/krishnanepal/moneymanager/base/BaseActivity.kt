package er.krishnanepal.moneymanager.base

import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.google.android.material.snackbar.Snackbar
import er.krishnanepal.moneymanager.R
import er.krishnanepal.moneymanager.utils.ConnectivityReceiver
import er.krishnanepal.moneymanager.utils.Loading
import er.krishnanepal.moneymanager.utils.SystemPrefManager
import er.krishnanepal.moneymanager.utils.Utils.getCurrentMode
import er.krishnanepal.moneymanager.utils.setupUI
import er.krishnanepal.onlineshopping.base.SuperBaseActivity

abstract class BaseActivity<DATA_BINDING : ViewDataBinding, VIEW_MODEL : BaseViewModel> : SuperBaseActivity(),
    ConnectivityReceiver.ConnectivityReceiverListener {

    var TAG: String = "BaseActivity"
    lateinit var binding: DATA_BINDING
    protected lateinit var viewModel: VIEW_MODEL

    private var progressBar: Loading? = null
    private var snackBar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        performDataBinding()
        registerReceiver(
            ConnectivityReceiver(), IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )
        progressBar = Loading(this)
        setupUI(binding.root)
    }

    @LayoutRes
    abstract fun getLayoutId(): Int

    abstract fun initViewModel(): VIEW_MODEL

    private fun performDataBinding() {
        binding = DataBindingUtil.setContentView(this, getLayoutId())
        setContentView(binding.root)
        this.viewModel = initViewModel()
        binding.apply {
            lifecycleOwner = this@BaseActivity
            executePendingBindings()
        }
    }

    private fun setUpTheme(){
        if (systemPrefManager[SystemPrefManager.SYSTEM_THEME] == getString(R.string.system_theme_light)) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        } else if (systemPrefManager[SystemPrefManager.SYSTEM_THEME] == getString(R.string.system_theme_dark)) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        } else {
            AppCompatDelegate.setDefaultNightMode(getCurrentMode(this))

        }
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showNetworkMessage(isConnected)
    }

    private fun showNetworkMessage(connected: Boolean) {
        if (!connected) {
//            ToastUtil.showCustomToast(this,"You are offline")
            snackBar =
                Snackbar.make(window.decorView.rootView, "You are offline", Snackbar.LENGTH_LONG)
//            snackBar = Snackbar.make(findViewById(R.id.rootLayout), "You are offline", Snackbar.LENGTH_LONG) //Assume "rootLayout" as the root layout of every activity.
//            snackBar?.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            snackBar?.show()
        } else {
//            snackBar =
//                Snackbar.make(window.decorView.rootView, "You are online", Snackbar.LENGTH_LONG)
//            snackBar?.show()

//            ToastUtil.showCustomToast(this,"You are online")

        }
    }

    override fun onResume() {
        super.onResume()
        ConnectivityReceiver.connectivityReceiverListener = this
    }

    override fun onDestroy() {
        super.onDestroy()
        if (progressBar?.isShowing == true) progressBar?.dismiss()
    }

    fun hideProgressBar() {
        if (progressBar?.isShowing == true) progressBar?.dismiss()
    }

    fun showProgressBar() {
        if (progressBar?.isShowing != true) progressBar?.show()
    }

}