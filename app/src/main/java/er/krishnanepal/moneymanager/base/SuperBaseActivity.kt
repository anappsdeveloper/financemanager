package er.krishnanepal.onlineshopping.base

import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import er.krishnanepal.moneymanager.utils.SystemPrefManager
import javax.inject.Inject

@AndroidEntryPoint
open class SuperBaseActivity : AppCompatActivity() {

    @Inject
    lateinit var systemPrefManager: SystemPrefManager

    @Inject
    lateinit var gson: Gson
}