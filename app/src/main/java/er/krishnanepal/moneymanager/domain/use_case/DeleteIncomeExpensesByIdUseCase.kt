package er.krishnanepal.moneymanager.domain.use_case

import er.krishnanepal.moneymanager.data.repo.IncomeExpensesRepo
import javax.inject.Inject

class DeleteIncomeExpensesByIdUseCase@Inject constructor(
    private val repo: IncomeExpensesRepo
){
    suspend operator fun invoke(id:Int) {
        repo.deleteIncomeExpensesEntityById(id)
    }
}