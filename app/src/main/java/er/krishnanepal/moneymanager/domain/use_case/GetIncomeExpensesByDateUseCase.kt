package er.krishnanepal.moneymanager.domain.use_case

import er.krishnanepal.moneymanager.data.local.IncomeExpensesEntity
import er.krishnanepal.moneymanager.data.repo.IncomeExpensesRepo
import java.util.*
import javax.inject.Inject

class GetIncomeExpensesByDateUseCase@Inject constructor(
    private val repo: IncomeExpensesRepo
) {
  suspend operator fun invoke(date: Date):List<IncomeExpensesEntity>/* =*/ /*flow*/{
      return  repo.getIncomeExpensesByDateEntity(date)/*.collect {
          *//*  emit(it)*//*
        }*/
    }
}