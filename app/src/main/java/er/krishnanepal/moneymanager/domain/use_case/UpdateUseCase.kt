package er.krishnanepal.moneymanager.domain.use_case

import er.krishnanepal.moneymanager.data.local.IncomeExpensesEntity
import er.krishnanepal.moneymanager.data.repo.IncomeExpensesRepo
import javax.inject.Inject

class UpdateUseCase  @Inject constructor(
    private val repo: IncomeExpensesRepo
){
    suspend operator fun invoke(incomeExpenses: IncomeExpensesEntity) {
        repo.updateIncomeExpensesEntity(incomeExpenses)
    }
}