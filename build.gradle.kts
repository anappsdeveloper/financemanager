buildscript {

    val kotlin_version = "1.8.10"

    val hilt_version = "2.44.2"
    val moshi_version = "1.13.0"
    val room_version = "2.4.1"
    val nav_version = "2.5.3"
    val coroutines_version = "1.5.2"
    val glide_version = "4.12.0"

    dependencies {
        classpath ("com.android.tools.build:gradle:7.1.0")
        classpath ("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version")
        classpath ("com.google.dagger:hilt-android-gradle-plugin:$hilt_version")
        classpath ("androidx.navigation:navigation-safe-args-gradle-plugin:$nav_version")
        classpath ("com.github.triplet.gradle:play-publisher:3.8.1")
        classpath(libs.google.services)

    }
}
// Top-level build file where you can add configuration options common to all sub-projects/modules.
plugins {
    id("com.android.application") version "8.2.2" apply false
//    id("org.jetbrains.kotlin.android") version "1.9.22" apply false
    id ("org.jetbrains.kotlin.android") version "1.7.0" apply false
    id ("com.android.library") version "7.3.1" apply false
    id ("com.google.android.libraries.mapsplatform.secrets-gradle-plugin") version "2.0.1" apply false
}
